# DevOps_School

Create a simple Web-application (see the description in the “Application” section below) and CI/CD infrastructure and pipeline for it.

Acceptance Criteria and presentation
A short presentation (.ppt or other) which contains description of the solution should be prepared and sent to the commission before a demo session.
The working application with the pipeline is to be demonstrated live on a “protection of the diploma” session for experts with comments and explanation of the details of the implementation, reasons of choosing tools and technologies.


1. Application sources should be placed in Git repository. Branching strategy should be explained.
2. CI pipeline may contain unit tests, smoke tests, linter check. 
3. CI/CD pipeline should use some quality/vulnerability control tool like a Sonar or Anchore.
4. CI/CI and runtime infrastructure should be described as a code using Terraform, CloudFormation, or any similar tool. On the demonstration deployment procedure should be shown.
5. All non cloud-native tools should be spinned up inside a K8S/OpenShift cluster inside a cloud. Application runtime environments should be inside the cluster too.
6. Infrastructure should have centralized log collection/display system. Logs of the application components and infra components should be collected.
7. Runtime infrastructure should have production and non production environments.  Deploy/release strategy should be explained.
8. Scalability should be provided and demonstrated
9. Report about the Cloud resource usage and the cost must be provided in the presentation. It should be efficient (minimal) – in accordance to the solving tasks. You can choose any cloud provider taking into account possible extra costs for the resources. As an example, you can use 3-workers node configuration with 1 CPU/8GB on each one in GCP under free tier.


Application
Develop a simple (lightweight) 3-tire application (front-end, back-end, database).

Back-end (collects data) must:
1. Retrieve a portion of data from API (see in your Variant) and store it in a database
2. Update data on demand
3. Update DB schema if needed on app’s update

Front-end (outputs data) must:
1. Display any portion of the data stored in the DB
2. Provide a method to trigger data update process

Database:
1. Choose Database type and data scheme in a suitable manner. 
2. Data must be stored in a persistent way
3. It’s better to use cloud native DB solutions like an RDS/AzureSQL/CloudSQL.
