from datetime import date, datetime
from flask import Flask, render_template, request
from calendar import monthrange
from flask.templating import render_template_string
from requests import get
import pymssql
import xml.etree.ElementTree as etree
import io
from os import getenv


def requesting_to(url):
    year = datetime.today().strftime('%Y')
    month = datetime.today().strftime('%m')
    last_day = monthrange(int(year), int(month))[1]
    date_end = str(last_day) + '/' + month + '/' + year
    date_start = '01' + '/' + month + '/' + year
    params = {'date_req1': date_start, 'date_req2': date_end}
    __name__ = get(url, params)
    return __name__.text


def parsing(xml_data):
    file = io.StringIO(xml_data)
    parser = etree.parse(file)
    root = parser.getroot()
    __name__ = list()
    for record in root.findall('Record'):
        date = record.get('Date')
        code = record.get('Code')
        buy_price = record.find('Buy').text
        sell_price = record.find('Sell').text
        if code == '1':
            metall = 'Gold'
        elif code == '2':
            metall = 'Silver'
        elif code == '3':
            metall = 'Platinum'
        else:
            metall = 'Palladium'
        __name__.append((date, code, metall, buy_price, sell_price))
    return __name__


def connecting():
    server = getenv('private_ip_address')
    user = "sqlserver"
    password = "sqlserver"
    database = getenv('name_database')
    __name__ = pymssql.connect(server, user, password, database)
    return __name__


def creating_table():
    conn = connecting()
    cursor = conn.cursor()
    cursor.execute("""
IF OBJECT_ID('dbo.metall', 'U') IS NULL
    CREATE TABLE metall (
        id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
        priceDate NVARCHAR(50) NOT NULL,
        code NVARCHAR(50) NOT NULL,
        metall NVARCHAR(50) NOT NULL,
        buyPrice NVARCHAR(50),
        sellPrice NVARCHAR(50))
    """)
    conn.commit()
    conn.close()


def inserting():
    conn = connecting()
    cursor = conn.cursor()
    data = requesting_to('http://www.cbr.ru/scripts/xml_metall.asp')
    cursor.executemany(
        'SELECT * FROM dbo.metall WHERE priceDate + code = %s + %s ',
        parsing(data))
    row = cursor.fetchone()
    if row is None:
        cursor.executemany(
            "INSERT INTO metall VALUES (%s, %s, %s, %s, %s)", parsing(data))
    conn.commit()
    conn.close()


app = Flask(__name__)


@app.route("/")
def index():
    creating_table()
    conn = connecting()
    cursor = conn.cursor()
    print(request.query_string.decode('UTF-8'))
    cursor.execute('SELECT * from dbo.metall ORDER BY code, priceDate')
    output_data = cursor.fetchall()
    if "action=update" in request.query_string.decode('UTF-8'):
        inserting()
        cursor.execute('SELECT * from dbo.metall ORDER BY code, priceDate')
        output_data = cursor.fetchall()
    if "action=delete" in request.query_string.decode('UTF-8'):
        cursor.execute('DELETE from dbo.metall')
        cursor.execute('SELECT * from dbo.metall ORDER BY code, priceDate')
        output_data = cursor.fetchall()
    return render_template('index.html', data=output_data)


@app.route("/health/ready")
def health():
    now = datetime.now()
    return  "<p>OK! YOU ARE WELCOME!</p>" + "<p>" + str(now.hour) + ":" + str(now.minute) + ":" + str(now.second) + "</p>"
